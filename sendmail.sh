#!/bin/bash
clear
set -u
#colores
v='\e[1;32m'
n='\e[1;30m'
r='\e[1;31m'
a='\e[1;34m'
am='\e[1;33m'
c='\e[1;36m'
b='\e[1;37m'
d='\e[0m'
ale=$(shuf -i 30-37 -n1)
at=$ale
ale1=$(shuf -i 30-37 -n1)
ats=$ale1

#protocolo_smpt
protsmtp='smtps://smtp.gmail.com:465'

#Banner
banner() {
printf "\e[1;${at}m"
figlet -t Send
sleep 1
printf "\e[1;${ats}m"
figlet -t Mail
sleep 1
printf "${n}Created by: ${c}F@br1x ${n}and ${c}你好😜${d}"|pv -qL 12
echo ""
echo ""
}
banner
#Datos
printf "${a}[${n}*${a}]${am}Ingrese su nombre de usuario:${b} "
read -r user
sleep 1
#verificar Gmail
while :
do
	printf "${a}[${n}*${a}]${am}Ingrese su correo electrónico:${b} "
	read -r correo
	sleep 1
	verifi=$(echo "${correo}"|grep -Po "@gmail.com")
	[ "${verifi}" == "@gmail.com" ] && break
	echo -e "${a}[${r}!${a}]${r}No es un correo de gmail ):"
	echo -e "${a}[${r}!${a}]${r}Favor ingresar nuevamente el correo!!"
done
printf "${a}[${n}*${a}]${am}Ingrese su contraseña:${b} "
read -r contra
echo ""
printf "${a}[${n}*${a}]${b}Pulsa enter para continuar"
read
clear

#verificacion_datos-Correo
banner
sleep 1
printf "\t${c}!!!Verificando datos de su correo!!!\n"
sleep 1
echo ""
printf "${a}[${v}+${a}]${b}Nombre Usuario: "
printf "${c}${user}\n"|pv -qL 12
sleep 1
printf "${a}[${v}+${a}]${b}Correo Electrónico: "
printf "${c}${correo}\n"|pv -qL 12
sleep 1
printf "${a}[${v}+${a}]${b}Contraseña: "
printf "${c}${contra}\n"|pv -qL 12
sleep 1
echo ""
printf "${n}1] ${v}Continuar\n"
printf "${n}2] ${v}Ingresar datos nuevamente\n${d}"
printf "${a}[${n}*${a}]${c}Escriba su opción>> ${am}"
read -r opcion

if [ "${opcion}" == "1" ] || [ "${opcion}" == "01" ]; then
	clear
	banner
elif [ "${opcion}" == "2" ] || [ "${opcion}" == "02" ]; then
	clear
	source 'sendmail.sh'
else
	printf "\n${a}[${r}!${a}]${r}opcion incorrecta ): bye ${d}\n"
	sleep 2
	exit
fi

envio() {
#Datos_envioCorreo
printf "\n${a}[${n}*${a}]${am}Ingrese su nombre:${b} "
read -r name
sleep 1
printf "\n${a}[${n}*${a}]${am}Ingrese el correo destinatario:${b} "
read -r destinatario
sleep 1
printf "\n${a}[${n}*${a}]${am}Ingrese el nombre del destinatario:${b} "
read -r named
sleep 1
printf "\n${a}[${n}*${a}]${am}Ingrese el Asunto del Correo:${b} "
read -r asunto
sleep 1
echo -e "\n${a}[${n}*${a}]${am}Ingrese el cuerpo del mensaje:${b} "
read -r mensaje
sleep 1
printf "\n${a}[${n}*${a}]${am}Ingrese su despedida del mensaje:${b} "
read -r despedida
sleep 1

#almacenando datos en archivo
touch correo.txt
echo "From: '${name}' <${correo}>" > correo.txt
echo "To: '${named}' <${destinatario}>" >> correo.txt
echo "Subject: ${asunto}" >> correo.txt
echo -e "\n${mensaje}" >> correo.txt
echo -e "\n${despedida}" >> correo.txt
}
envio
echo ""
printf "${n}1] ${v}Continuar\n"
printf "${n}2] ${v}Ingresar datos nuevamente\n${d}"
printf "${a}[${n}*${a}]${c}Escriba su opción>> ${am}"
read -r opci
if [ "${opci}" == "1" ] || [ "${opci}" == "01" ]; then
	clear
	banner
	sleep 1
elif [ "${opci}" == "2" ] || [ "${opci}" == "02" ]; then
	clear
	banner
	envio
else
	printf "\n${a}[${r}!${a}]${r}opcion incorrecta ): bye ${d}\n"
	sleep 2
	exit
fi

#verificar datos de envio
printf "\n\t${c}!!!Verificando datos de su mensaje!!!\n${v}"|pv -qL 15
echo ""
cat correo.txt|pv -qL 15
sleep 2
echo ""
printf "${n}1] ${v}Continuar\n"
printf "${n}2] ${v}Ingresar datos nuevamente\n${d}"
printf "${a}[${n}*${a}]${c}Escriba su opción>> ${am}"
read -r opc

if [ "${opc}" == "1" ] || [ "${opc}" == "01" ]; then
        clear
        banner
        sleep 1
	echo -e "${a}[${v}+${a}]${am}Enviando mensaje...${d}"|pv -qL 12
	sleep 1
	curl -n --ssl-reqd --mail-from "${user}" --mail-rcpt "${destinatario}" --url ${protsmtp} -u "${correo}:${contra}" -T correo.txt
	echo -e "${a}[${v}+${a}]${v}Mensaje enviado exitosamente al correo:${am}${destinatario}!! ${d}"|pv -qL 12
	sleep 2
	exit
elif [ "${opc}" == "2" ] || [ "${opc}" == "02" ]; then
        clear
        banner
        envio
else
        printf "\n${a}[${r}!${a}]${r}opcion incorrecta ): bye ${d}\n"
        sleep 2
        exit
fi
